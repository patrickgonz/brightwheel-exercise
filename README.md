This app displays the top starred 100 github respositories. The data for each repo is displayed in card.
The following data is displayed:
	- Repo name
	- Repo URL
	- Repo stars
	- Repo commits in the last 24 hours, with the following data:
		- Name of Author
		- Date of commit
		- Commit message

TODO:
1. Since there is a request limit imposed by GitHub for unauthenticated request of 60 request per hour. You 
must authenticate to increse the request limit to 5000 per hour. To do so you could do the following:
	a. Use Auth0 and connect your app to GitHub - https://auth0.com/docs/connections/social/github
	b. Use Auth0 Single Page Application SDK to set us authentication - https://auth0.com/docs/libraries/auth0-spa-js
	c. Basic inforamtion on Auth0 can be found here - https://auth0.com/docs/getting-started/the-basics

2. Add validation of arguments to increase security and prevent user errors. Test the type of parameters using typeof

File Descriptions:
HTML
index.html - simple html used to display the data. It has a simple header and footer with a placeholder div where the cards are added.

CSS
reset.css - Used to reset the browser defaults
main.css - common/main css rules, contains rules for cards

JavaScript
githubdata.js - contains the GitHubData class. This class handles creating the queries from github, sends the get requests and 
calls a event handler to process data (create the cards). Provides a method to get the commit data that needs to be displayed.

dataprocessor.js - contains callback functions and onload event function

cardfactory - contains the CardFactory class. This class handles the building of the cards. Contains the CommitSection class which handles building the commit section for each repo.

Test
testApi.html - used to test the GitHubData class.