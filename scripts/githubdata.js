/* GitHubData class                                                       */
/* Provides functionality to obtain data from github using the github API */
/* Copyright © 2019 Patrick Gonzalez                                      */

class GitHubData
{       
	constructor()
	{
		//FUTURE: Provide getters and setters to allow users to set different search parameters
		this.searchRepoString = "https://api.github.com/search/repositories";
		this.searchCommitString = "https://api.github.com/repos";
		this.pageCount = 1;
		this.maxPerPageCount = 100;
		this.minStars = 5000;
	}

	// Searches for the top starred repositories
	searchTopStarredRepos()
	{
		// Build the search string to get the top starred repos. Limit the request to the number of pages and per page count specified in constructor.
		var searchString = this.searchRepoString + "?q=stars:>=" + this.minStars + "&per_page="  + this.maxPerPageCount + "&page=" + this.pageCount;

		var xhttpObj = new XMLHttpRequest();

		// Event handler to handle the processing of the data received by the search repo request
		xhttpObj.onreadystatechange = function()
		{
			// When data is response is ready and request is successful
			if (this.readyState === 4 && this.status === 200)
			{
				// FUTURE: Provide a way to mechanism for logging data
				var jsonData = JSON.parse(this.responseText);

				// Used for testing
				//console.log(jsonData);
				processRepoData(jsonData);
			}

		};

		// Build request
		xhttpObj.open("GET", searchString, true);

		// Send request
		xhttpObj.send();
	}

	// Searched the commits for a repo
	// Arguments: owner - the name of the owner of the repo
	//			  name - the name of the repo
	//			  hours - how many hours back do you want to search for commits from the current date
	searchCommitInformation(owner, name, hours)
	{
		// Calculate date
		var date = new Date();
		date.setHours(date.getHours() - hours);

		// Build the search string to get the repos commit information. Limit the request to the last hours specifed by the hours argument.
		var searchString = this.searchCommitString + "/" + owner + "/" + name + "/commits?since=" + date.toISOString();

		var xhttpObj = new XMLHttpRequest();

		// Event handler to handle the processing of the data received by the search commit request
		xhttpObj.onreadystatechange = function()
		{
			// When data is response is ready and request is successful
			if (this.readyState === 4 && this.status === 200)
			{
				//Save data in object so it can be used by consumer class
				//FUTURE: Provide a way to mechanism for logging
				processCommitData(name, JSON.parse(this.responseText));
			}
			// FUTURE - add error logic for other status
			else if (this.readyState === 4 && this.status === 403)
			{
				processCommitDataError(name);
			}

		};

		// Build request
		xhttpObj.open("GET", searchString, true);

		// Send request
		xhttpObj.send();
	}

}
