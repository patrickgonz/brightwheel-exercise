/* CardFactory class                                                     */
/* Provides functionality to build the cards to be shown on the web page */
/* Copyright © 2019 Patrick Gonzalez                                     */
class CardFactory
{
	constructor(jsonData)
	{
		this.jsonData = jsonData;
		this.itemArray = jsonData.items;
	}

	// Builds one card from the arguments provided
	// Arguments: item - the repo item from the array
	//			  index - the index of the item in the array
	//			  itemArray - the array object
	buildCard(item, index, itemArray)
	{
		// Use HTML template to add card to DOM
		// Test for browser support of template tag
		if ('content' in document.createElement('template'))
		{
			// Get the card template
			var template = document.getElementById('cardTemplate');

			// Create a clone from the template content
			var clone = document.importNode(template.content, true);

			// Set the variable data from GitHub into the clone
			// This id is used to add the commit data. Assumes the item.name is unique.
			clone.querySelector('.brightwheel.card').id = item.name;		
			clone.querySelector('.avatarImage').src = item.owner.avatar_url;
			clone.querySelector('.nameDiv').textContent = item.name;
			clone.querySelector('.urlDiv').textContent = item.html_url;
			clone.querySelector('.starsDiv').textContent = item.stargazers_count;

			// Append card as child of cards_placeholder div
			document.getElementById('cards_placeholder').append(clone);

			// Get commit data
			getCommitData(item.owner.login, item.name, 24);

		}
		else 
		{
			// FUTURE: Use a different method
		}
	}

	// Loops through items array to build the cards
	buildCards()
	{
		this.itemArray.forEach(this.buildCard);
	}
	
}


/* CommitSection class                                                   */
/* Provides functionality to build the commit section in the cards       */
/* FUTURE: Move to its own file when it gets used elsewhere              */
class CommitSection
{
	constructor(name, jsonData)
	{
		this.name = name;
		this.jsonData = jsonData;
	}

	// Builds the commit section for a repo in the repo's card
	buildCommitSection()
	{
		var card = document.getElementById(this.name);

		var elements = card.getElementsByClassName('commitSection');

		//Loop through data and append elements
		if(this.jsonData.length != 0)
		{
			this.jsonData.forEach(function(item)
			{
				// FUTURE: Optimization, resuability and maintenance, move this to a template
				// Add elements to DOM containing author name
				var span1 = document.createElement('span');
				span1.append('Author:');
				elements.item(0).append(span1);

				var div1 = document.createElement('div');
				div1.append(item.commit.author.name);
				elements.item(0).append(div1);

				// Add elements to DOM containing commit date
				var span2 = document.createElement('span');
				span2.append('Date:');
				elements.item(0).append(span2);

				var div2 = document.createElement('div');
				var date = new Date(item.commit.author.date);
				var dateString = date.toString();
				div2.append(dateString);
				elements.item(0).append(div2);

				// Add elements to DOM containing commit message
				var span3 = document.createElement('span');
				span3.append('Message:');
				elements.item(0).append(span3);

				var div3 = document.createElement('div');
				div3.append(item.commit.message);
				elements.item(0).append(div3);
			});
		}
		else
		{
			var p = document.createElement('p');
			p.append("No commits in the last 24 hours.");
			elements.item(0).append(p);
		}
	}

	// Builds the commit section if an error occurs
	buildCommitError(name)
	{
		var card = document.getElementById(this.name);

		var elements = card.getElementsByClassName('commitSection');

		// Add elements to DOM with an error message
		var div1 = document.createElement('div');
		div1.append("Authentication Error.");
		elements.item(0).append(div1);
	}
}
