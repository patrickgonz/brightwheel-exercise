/* Helper functions to handle the processing of data and to add the cards */
/* Copyright © 2019 Patrick Gonzalez                                      */

// FUTURE - remove the gitHubDataObj global variable by using the module pattern
// This is done by wrapping the code in an anonymous function and returning pointers to the functions
// and inside the function you can declare a var for gitHubDataObj

// Function that its called to start the processing of getting the data and building the cards
function addCards()
{
	gitHubDataObj = new GitHubData();
	gitHubDataObj.searchTopStarredRepos();
}

// The next 3 functions where created to decouple the GitHubData class from the CardFactory class.
// This allows changing of the implementiation without having to make changes to both classes.

// Used to process the repo data
function processRepoData(jsonData)
{
	var cardFactoryObj = new CardFactory(jsonData);

	cardFactoryObj.buildCards();
}

// Used to get the commit data
function getCommitData(owner, name, hours)
{
	gitHubDataObj.searchCommitInformation(owner, name, hours);
}

// Used to process the commit data
function processCommitData(name, data)
{
	var commitSectionObj = new CommitSection(name, data);

	commitSectionObj.buildCommitSection();
}

// Used to handle errors getting the commit data 
function processCommitDataError(name)
{
	var data = null;
	var commitSectionObj = new CommitSection(name, data);

	commitSectionObj.buildCommitError();
}
